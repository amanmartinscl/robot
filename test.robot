*** Settings ***
Library           SeleniumLibrary
Library           Dialogs
Library           Collections

*** Test Cases ***
test01
    Open Browser    http://localhost:3000/    chrome
    Maximize Browser Window
    loop
    ${list_pos}=    My list
    ${list_pre}=    My list
    sort a list ${list_pre}
    ${status}=    sort validation ${list_pos} and ${list_pre}
    validation pro ${status}
    Pause Execution
    [Teardown]    Close All Browsers

*** Keywords ***
sort validation ${list1} and ${list2}
    ${i}    Set Variable    1
    ${qtd}    Get Element Count    //div[@id="app"]/ul/li
    : FOR    ${i}    IN RANGE    ${qtd}
    \    Run Keyword If    '${list1[${i}]}' != '${list2[${i}]}'    Return From Keyword    different
    \    ${i}=    Set Variable    '${i+1}'

My list
    ${qtd}    Get Element Count    //div[@id="app"]/ul/li
    @{list}    Create List
    : FOR    ${i}    IN RANGE    ${qtd}
    \    ${value}    Get Text    xpath=//div[@id="app"]/ul/li[${i}+1]
    \    Append To List    ${list}    ${value}
    \    ${i}=    Set Variable    '${i+1}'
    Return From Keyword    ${list}

sort a list ${list}
    Sort List    ${list}

loop
    ${qtd}    Get Element Count    //div[@id="app"]/ul/li
    ${list_pos}=    Create List
    ${list_pre}=    Create List
    ${j}=    Set Variable    1
    ${i}=    Set Variable    1
    : FOR    ${i}    IN RANGE    ${qtd-1}
    \    ${value}    Get Text    xpath=//div[@id="app"]/ul/li[${i}+1]
    \    ${value2}    Get Text    xpath=//div[@id="app"]/ul/li[${i}+2]
    \    Run Keyword If    '${value}' < '${value2}'    log    Menor    warn
    \    Run Keyword If    '${value}' > '${value2}'    Drag And Drop    xpath=//div[@id="app"]/ul/li[${i}+1]    xpath=//div[@id="app"]/ul/li[${i}+2]
    \    ${i}=    Set Variable    '${i+1}'

validation pro ${status}
    @{xpto}    Create List    different
    : FOR    ${var}    IN    @{xpto}
    \    Run Keyword If    '${status}' == 'different'    loop
    \    ${list_pos}=    My list
    \    ${list_pre}=    My list
    \    sort a list ${list_pre}
    \    ${status}=    sort validation ${list_pos} and ${list_pre}
    \    Run Keyword If    '${status}' == 'different'    validation pro ${status}
    ${final_results}=    My list
    Log list    ${final_results}    warn
